# Sample Hardhat Project

Template project

## Notes

- Run `npm install` to install all the dependencies. There are some vulnerabilities warnings, but as this is not a deployed project, it's not a problem. 

- Create a `.env` file using the format from `.env_example` and fill in the values.

- Add the files you don't want tracked to `.gitignore`. This includes ALL the files that include your private keys without using `.env`. **DON'T COMMIT PRIVATE KEYS**

- This template uses Sepolia Testnet and assumes the use of [Alchemy](https://www.alchemy.com/) for the API (Create an API for Sepolia Testnet there). You can change this in `hardhat.config.js`.

- Use a wallet only for testing purposes, don't use your main wallet.

## Relevant commands

- `npx hardhat compile` to compile the contracts
- `npx hardhat run scripts/deploy.js` to deploy the contracts to Localnet 
- `npx hardhat run scripts/deploy.js --network sepolia` to deploy the contracts to Sepolia Testnet
- `npx hardhat test` to run the tests (ONLY works on Localnet)

## Example Setup
(you don't need to follow the names, but keep them consistent)

- Using [Alchemy](https://www.alchemy.com/), add to `.env` your `API_KEY` and `API_URL`. The `API_URL` has the `API_KEY` at the end, so you can really just use `API_URL`.

- Add your [Metamask](https://metamask.io/) or any other test wallet private key to `.env` as `PRIVATE_KEY`.

- The default contract in this template is `contracts/Lock.sol`, with tests in `test/Lock.ts`.

- The default deployment script is `scripts/deploy.js`. You can modify this for your needs.

## Extras

- Decent faucet (needs twitter auth, 1 time per hour) [Chainlink - Sepholia](https://vrf.chain.link/sepolia/new)
- You could need a free [Chainlink VRF Sub](https://vrf.chain.link/sepolia)

## Relevant Docs

- [Alchemy overview of Sepolia Testnet](https://www.alchemy.com/overviews/sepolia-testnet)
- [Deploy contract with Alchemy](https://docs.alchemy.com/docs/how-to-deploy-a-smart-contract-to-the-sepolia-testnet)
- [More detailed docs and template for hardhat](https://github.com/smartcontractkit/hardhat-starter-kit/blob/main/README.md#using-a-testnet-or-live-network-like-mainnet-or-polygon)