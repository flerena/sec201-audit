1- **Validation**  The contract does not validate the input parameters of the `addValidator` fx (Line 72), so although the function is restricted to the owner, it is possible to add a contract as a validator. (May or may not be intended or a security risk)

2- **Validation not used** In Line 27 there's a definition for `onlyEOA()` modifier, but its not used, so we can assume that the previous point is intended to be an error, not a feature.

3- **Reentrancy** the `unstake` function calls `_unstake`, and `_unstake` calls `_deleteFromValidators` and change the state of the contract. If a contract is a validator, it could trigger a reentrancy attack by calling `unstake` again. This could be prevented using the `nonReentrant` modifier if a contract is needed to be a validator, otherwise, the `onlyEOA` modifier should be used.


4- **Overflow** the `stake` function calls `_stake`. This adds `msg.value` to `_stakedAmount` (Line 78). If `_stakedAmount` is close to the maximum value of `uint256`, it could overflow. This could be prevented by using the `SafeMath` lib. This is an enourmous amount of ETH, so it is unlikely to happen, but still.

5- **Error** (Line 79) should be
 
```solidity
    _addressToStakedAmount[msg.sender] += msg.value;
```

6- **Error** (Line 89) should be 

```solidity
    !_addressToIsValidator[msg.sender] && ...
```

 
7- **DOS** at Line 15 its defined 
```solidity 
address[] public _validators;
```
An atacker could create multiple 1 ETH validators and make the contract unusable for gas limit reasons. This could be prevented by using a mapping instead of an array.

8- **Breaker** The contact does not have a breaker function, so if a bug is found after deploy, it cannot be stopped.  

9- **Error messages** 
  - Line 97 `access denied` isn't explanatory enough.
  

10- **Reentracy** (Line 106) 
```solidity 
        payable(msg.sender).transfer(amount);
        _addressToStakedAmount[msg.sender] = 0;
        _stakedAmount -= amount;
        emit Unstaked(msg.sender, amount);
```

This doesn't follow the check-effect-interaction pattern, so its vulnerable to reentrancy attacks. Stick to the pattern or use the `nonReentrant` modifier.


