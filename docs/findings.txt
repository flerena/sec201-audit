Staking.sol

1-
onlyEOA line 27
It is not used. 

2-
addValidator line 72 (video)
New validators can be added without staking any ether. _addressToIsValidator and _addressToStakedAmount are not being updated.

3-
_stake line 79
Should be _addressToStakedAmount[msg.sender] += msg.value;

4-
_stake line 82
Should be !_addressToIsValidator[msg.sender] within the if statement.

5-
_unstake line 94
A smart contract can execute unstake(). Should be set to onlyEOA().

6-
_unstake line 97
The require statement could have a more explanatory message. e.g:
"Number of validators can't be less than MinimumRequiredNumValidators".

7-
_unstake line 106
Doesnt follow check-effect-interaction pattern. Vulnerable to reentrancy attack.

8-
_deleteFromValidators line 128
Address to validator should be set to false.


