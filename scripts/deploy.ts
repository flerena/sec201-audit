import { ethers } from "hardhat";
import dotenv from 'dotenv';

var ContractNames = ["ETBDex","ETBToken"];

async function main() {

  const Contract = await ethers.getContractFactory(ContractNames[0]);
  const contract = await Contract.deploy();

  await contract.deployed();
  console.log(`${ContractNames[0]} deployed to ${contract.address}`);


  const Contract2 = await ethers.getContractFactory(ContractNames[1]);
  const contract2 = await Contract.deploy();

  await contract.deployed();
  console.log(`${ContractNames[1]} deployed to ${contract2.address}`);


}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
